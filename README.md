# HackLab Cormano Hugo Theme
A [Hugo](https://gohugo.io) theme initially based on the [Bootstrap v4 blog example](https://getbootstrap.com/docs/4.0/examples/blog), forked from [Hugo Bootstrap 4 Blog Theme](https://github.com/alanorth/hugo-theme-bootstrap4-blog).

See [HackLab Cormano on GitLab Pages](https://hacklabcormano.gitlab.io/hacklabcormano.it/) for an example of this theme in use.

## Features

- Responsive design
- Uses Bootstrap v4's [native system font stack](https://getbootstrap.com/docs/4.0/content/reboot/#native-font-stack) to load quickly and look good on all platforms
- Basic [OpenGraph](http://ogp.me) and [Twitter Card](https://dev.twitter.com/cards/types) metadata support
- robots.txt linking to XML sitemap (disabled by default, see [Hugo docs](https://gohugo.io/extras/robots-txt/))
- Basic support for [multi-lingual content](https://github.com/spf13/hugo/blob/master/docs/content/content/multilingual.md) (added in Hugo 0.17)
- Supports Google, Bing, and Yandex site verification via meta tags
- Supports Google Analytics (async version), see [Hugo docs](https://gohugo.io/extras/analytics/)
- Supports Disqus comments, see [Hugo docs](https://gohugo.io/extras/comments/)
- Can show a message about cookie usage to the user, see [`exampleSite/config.toml`](https://gitlab.com/HackLabCormano/hacklabcormano-hugo-theme/blob/master/exampleSite/config.toml)
- Allow addition of custom `<head>` code in site's `layouts/partial/head-custom.html`
- Configurable display of summaries of content in list templates.

## Usage
Clone the repository to your site's `themes` directory. Refer to [`exampleSite/config.toml`](https://gitlab.com/HackLabCormano/hacklabcormano-hugo-theme/blob/master/exampleSite/config.toml) for recommended configuration values.

## Content Suggestions
A few suggestions to help you get a good looking site quickly:

- Keep blog posts in the `content/post` directory, for example: `content/post/my-first-post.md`
- Keep static pages in the `content` directory, for example: `content/about.md`
- Keep media like images in the `static` directory, for example: `static/2016/10/screenshot.png`
- If you want an image to be shown when you share a post on social media, specify at least one image in the post's front matter, for example: `images: ["/2016/10/screenshot.png"]`
- Use the `<!--more-->` tag in posts to control how much of a post is shown on summary pages
- Disable comments on a post by setting `comments = false` in its frontmatter
- Disable social sharing icons site wide (or on individual pages/posts) by setting `sharingicons = false`
- If your content is stored in git, add `enableGitInfo = true` to your site config and Hugo will use git history to set a more accurate modification date in page metadata

## Building (For Developers)
This theme uses the [Bootstrap](https://getbootstrap.com/) framework. A static version of this is already included, but if you want to bump the version, tweak the style, etc, you'll need to rebuild the assets. Make sure you have NodeJS >= v6 installed, and then run the following from inside the theme's directory:

```console
$ npm install
$ npm run build
```

## Contributing
There are several ways to help with the development of the theme:

- Fork [the repository](https://gitlab.com/HackLabCormano/hacklabcormano-hugo-theme.git) on GitLab, add features on a "feature" branch like `update-bootstrap`, and then send a pull request with your changes

## License
This repository contains the code of [Bootstrap](http://getbootstrap.com), which is licensed under the [MIT license](https://tldrlegal.com/license/mit-license), and [Font Awesome](http://fontawesome.io/), which uses [various licenses](http://fontawesome.io/license/).

Otherwise, the contents are [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt).
